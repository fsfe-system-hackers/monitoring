<!--
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2019 Free Software Foundation Europe
-->

# FSFE monitoring

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/monitoring/00_README)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/monitoring)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/monitoring)
[![Build Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/monitoring/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/monitoring)

The goal is to deploy incinga 2 and the large part of its server configuration.

## What it does?

### On the sever

1. Install icinga.
2. Deploy the configuration
3. Deploy the web interface and the apache configuration

**Note**: As of now the ansible playbook does not handle the configuration of
the web interface itself, it's not currently supported by the official ansible
role. Furthermore, the official [Ansible collection](https://github.com/Icinga/ansible-collection-icinga)
does lots of things and is a great starting point for a fully automated Icinga
stack, but, however, is hard to use to replace a previously manually configured
Icinga 2. Thus, it will be used for installation, but further work is
semi-manually.

### On the clients

**Note**: This playbook does not store the *hosts* configuration, and does not
set up the monitored clients. For workflow reasons this happens via the
[baseline](https://git.fsfe.org/fsfe-system-hackers/baseline) playbook.

1. Deploy the SSH public key
2. Create the icinga user

## How to use it?

**Note**: Please make sure you have ssh access to the servers listed in the
`icinga2_clients` section of the
[inventory](https://git.fsfe.org/fsfe-system-hackers/inventory).

Clone this repo:
``` bash
git clone --recurse-submodules git@git.fsfe.org:fsfe-system-hackers/monitoring.git
```

Update the `icinga.icinga` Ansible collection from Ansible Galaxy:
```bash
ansible-galaxy install -r requirements.yml --force
```

Update the [inventory
submodule](https://git.fsfe.org/fsfe-system-hackers/inventory) to reflect the
newest changes to the list of our hosts and the groups that they are in:
``` bash
git submodule update --remote inventory
```

The following commands are most used:

Configure only the server:

```bash
ansible-playbook playbook.yml -l "icinga2_server"
```

Only deploy a changed config to the server:

```bash
ansible-playbook playbook.yml -l "icinga2_server" -t conf
```

To update clients and the `hosts.conf` file, have a look at the
[baseline](https://git.fsfe.org/fsfe-system-hackers/baseline) repository.


**Note**: You have to manually add sensitive files which are not in this repo but only on the host: `/etc/icinga2/id_rsa` and `/etc/msmtprc`!

The user facing doc is
[here](https://wiki.fsfe.org/TechDocs/TechnicalProcesses/Monitoring). The
official documentation is
[here](https://icinga.com/docs/icinga2/latest/#start-with-icinga).

## How does it work?

The `templates/` directory contains the apache virtual hosts configuration.  The
`roles/` folder contains the upstream code for icinga2 installation.  The
`conf.d/` folder contains the icinga2 configuration itself.

The address of the icinga2 server and the clients are configured in the
`inventory/inventory.txt` file.
