<!--
SPDX-License-Identifier: GPL-3.0-or-later
Copyright © 2019 Free Software Foundation Europe
-->

This directory contains custom monitoring plugins meant to be used directly from
the icinga server
