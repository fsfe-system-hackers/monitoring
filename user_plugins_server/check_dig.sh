#!/usr/bin/env bash

# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe e.V. <https://fsfe.org>

# This is a simple script for Icinga/nagios to test whether the reply of
# a DNS server matches an expected value. It relies on dig (dnsutils)

while getopts d:e:s: OPTIONS; do
  case $OPTIONS in
    d) DOMAIN=$OPTARG;;
    e) EXPECTED=$OPTARG;;
    s) SERVER=$OPTARG;;
    *) echo "Invalid Command Line Option";;
  esac
done

if [[ -z "${DOMAIN}" || -z "${EXPECTED}" || -z "${SERVER}" ]]; then
  echo "UNKNOWN - arguments -d and/or -e and/or -s missing"
  exit 3
fi

# make a dig request against the given DNS server for the given domain
# request the A and AAAA record
# Check with grep whether the expected value has been returned
OUT="$(dig +short @"${SERVER}" "${DOMAIN}" A "${DOMAIN}" AAAA)"
STAT_DIG=$?
STAT_FOUND=$(echo "${OUT}" | grep -qE "$EXPECTED"; echo $?)
if [[ ( $STAT_DIG == 0 ) && ( $STAT_FOUND == 0 ) ]]; then
  echo "OK - ${SERVER} returned the expected ${EXPECTED} for ${DOMAIN}"
  exit 0
else
  echo "CRITICAL - ${SERVER} returned \"$(echo "${OUT}" | sed -z 's/\n/,/g;s/,$/\n/')\" instead of the expected ${EXPECTED}"
  exit 2
fi
