#!/bin/bash
#
# Copyright (c) 2019 Ekhozie (ekhozie@users.noreply.github.com)
# Copyright (c) 2021 Free Software Foundation Europe <https://fsfe.org>
# SPDX-License-Identifier: GPL-3.0-or-later

TARGET='https://fsfe.org'
MAX_TIME=10

function print_help() {
    cat <<EOF
check_socks - Monitor a host via torsocks
Copyright (c) 2019 Ekhozie (ekhozie@users.noreply.github.com)
Copyright (c) 2021 Free Software Foundation Europe <https://fsfe.org>
Usage:
    --help     = Print this message
    -T         = Url to check (default https://fsfe.org)
    --max-time = Maximum time in seconds that you allow the whole operation to take (default 10)
EOF
}

OPTS=$(getopt -o hT: --long max-time:,help -n 'check_tor' -- "$@")

if [[ $? != 0 ]] ; then
    echo "UNKNOWN - Failed parsing options."
    exit 3
fi

eval set -- "$OPTS"

while true; do
  case "$1" in
    -T ) TARGET="$2"; shift; shift ;;
    --max-time ) MAX_TIME="$2"; shift; shift ;;
    -h | --help ) print_help; exit 0; shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

if [[ -z $TARGET ]] ; then
    echo 'UNKNOWN - Missing required parameter(s).'
    exit 3
fi

ts=$(date +%s%N)
torsocks curl --max-time "${MAX_TIME}" -sf "${TARGET}" &>/dev/null
ret=$?
tt=$((($(date +%s%N) - $ts)/1000000))

if [[ $ret -eq 0 ]] ; then
    echo "OK - Latency ${tt}ms."
    exit 0
else
    echo "CRITICAL - Target is not reachable, curl returns $ret after ${tt}ms"
    exit 1
fi
