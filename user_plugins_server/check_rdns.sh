#!/usr/bin/env bash

# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2021 Free Software Foundation Europe e.V. <https://fsfe.org>

# This is a simple script for Icinga/nagios to test whether a given hostname
# contains a PTR record for its IPv4 and IPv6

while getopts d: OPTIONS; do
  case $OPTIONS in
    d) DOMAIN=$OPTARG;;
    *) echo "Invalid Command Line Option";;
  esac
done

if [[ -z "${DOMAIN}" ]]; then
  echo "UNKNOWN - argument -d missing"
  exit 3
fi

# make a dig request to get all IPv4 and IPv6 for the given domain
# Then, check whether the output contains the domain name
IPS="$(dig +short "${DOMAIN}" A "${DOMAIN}" AAAA)"
STAT_DIG=$?

failed_ips=
ok_ips=
if [[ $STAT_DIG == 0 ]]; then
  for ip in $IPS; do
    if ! dig +short -x ${ip} | grep -qE ${DOMAIN}; then
      failed_ips="${failed_ips}, ${ip}"
    else
      ok_ips="${ok_ips}, ${ip}"
    fi
  done
  # if any PTR for an IP failed
  if [[ -n $failed_ips ]]; then
    echo "WARNING - following IPs of ${DOMAIN} do not have a PTR record: $(echo $failed_ips | sed -E 's/^, //')"
    exit 1
  else
    echo "OK - all IPs of ${DOMAIN} have a PTR record: $(echo $ok_ips | sed -E 's/^, //')"
    exit 0
  fi
# if dig failed
else
  echo "CRITICAL - dig failed to get A and AAAA record for ${DOMAIN}"
  exit 2
fi
