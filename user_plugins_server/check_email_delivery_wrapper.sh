#!/bin/bash

# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe e.V. <https://fsfe.org>

# This is a wrapper script for check_email_delivery which is custom to
# the setup of this instance. It reads the password from the msmtp file
# and just provides options for the most necessary settings.

while getopts P:w:S:I:U:m:t:f: OPT; do
  case $OPT in
    P)  PLUGINPATH=$OPTARG;;
    w)  WAIT=$OPTARG;;
    S)  SMTPSERVER=$OPTARG;;
    I)  IMAPSERVER=$OPTARG;;
    U)  MAILUSER=$OPTARG;;
    m)  MSMTPFILE=$OPTARG;;
    t)  MAILTO=$OPTARG;;
    f)  MAILFROM=$OPTARG;;
    *)  echo "Unknown option: -$OPTARG"; exit 1;;
  esac
done

MAILPASS=$(grep -C2 "user\s*$MAILUSER" "${MSMTPFILE}" | grep "password" | awk '{print $2}')

"${PLUGINPATH}"/check_email_delivery --wait ${WAIT} \
  --plugin "${PLUGINPATH}/check_smtp_send -H ${SMTPSERVER} --ssl -U ${MAILUSER} -P ${MAILPASS} --body 'Icinga Test Mail' --header 'Subject: Icinga Check %TOKEN1%' --mailfrom ${MAILFROM} --mailto ${MAILTO}" \
  --plugin "${PLUGINPATH}/check_imap_receive -H ${IMAPSERVER} --ssl -U ${MAILUSER} -P ${MAILPASS} -m Icinga -s SUBJECT -s 'Icinga Check %TOKEN1%'"
exit $?
