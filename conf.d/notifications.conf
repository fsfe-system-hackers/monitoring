# SPDX-FileCopyrightText: 2020 Vincent Lequertier <vincent@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

/* Notification Commands
 *
 * Please check the documentation for all required and
 * optional parameters.
 */

object NotificationCommand "mail-host-notification" {
  command = [ ConfigDir + "/scripts/mail-host-notification.sh" ]

  arguments += {
    "-4" = "$notification_address$"
    "-6" = "$notification_address6$"
    "-b" = "$notification_author$"
    "-c" = "$notification_comment$"
    "-d" = {
      required = true
      value = "$notification_date$"
    }
    "-f" = {
      value = "$notification_from$"
      description = "Set from address. Requires GNU mailutils (Debian/Ubuntu) or mailx (RHEL/SUSE)"
    }
    "-i" = "$notification_icingaweb2url$"
    "-l" = {
      required = true
      value = "$notification_hostname$"
    }
    "-n" = {
      required = true
      value = "$notification_hostdisplayname$"
    }
    "-o" = {
      required = true
      value = "$notification_hostoutput$"
    }
    "-r" = {
      required = true
      value = "$notification_useremail$"
    }
    "-R" = {
      value = "$notification_replyto$"
    }
    "-s" = {
      required = true
      value = "$notification_hoststate$"
    }
    "-t" = {
      required = true
      value = "$notification_type$"
    }
    "-v" = "$notification_logtosyslog$"
  }

  vars += {
    notification_address = "$address$"
    notification_address6 = "$address6$"
    notification_author = "$notification.author$"
    notification_comment = "$notification.comment$"
    notification_type = "$notification.type$"
    notification_date = "$icinga.long_date_time$"
    notification_hostname = "$host.name$"
    notification_hostdisplayname = "$host.display_name$"
    notification_hostoutput = "$host.output$"
    notification_hoststate = "$host.state$"
    notification_useremail = "$user.email$"
    notification_icingaweb2url = "https://monitoring.fsfe.org"
  }
}

object NotificationCommand "mail-service-notification" {
  command = [ ConfigDir + "/scripts/mail-service-notification.sh" ]

  arguments += {
    "-4" = "$notification_address$"
    "-6" = "$notification_address6$"
    "-b" = "$notification_author$"
    "-c" = "$notification_comment$"
    "-d" = {
      required = true
      value = "$notification_date$"
    }
    "-e" = {
      required = true
      value = "$notification_servicename$"
    }
    "-f" = {
      value = "$notification_from$"
      description = "Set from address. Requires GNU mailutils (Debian/Ubuntu) or mailx (RHEL/SUSE)"
    }
    "-i" = "$notification_icingaweb2url$"
    "-l" = {
      required = true
      value = "$notification_hostname$"
    }
    "-n" = {
      required = true
      value = "$notification_hostdisplayname$"
    }
    "-o" = {
      required = true
      value = "$notification_serviceoutput$"
    }
    "-r" = {
      required = true
      value = "$notification_useremail$"
    }
    "-R" = {
      value = "$notification_replyto$"
    }
    "-s" = {
      required = true
      value = "$notification_servicestate$"
    }
    "-t" = {
      required = true
      value = "$notification_type$"
    }
    "-u" = {
      required = true
      value = "$notification_servicedisplayname$"
    }
    "-v" = "$notification_logtosyslog$"
  }

  vars += {
    notification_address = "$address$"
    notification_address6 = "$address6$"
    notification_author = "$notification.author$"
    notification_comment = "$notification.comment$"
    notification_type = "$notification.type$"
    notification_date = "$icinga.long_date_time$"
    notification_hostname = "$host.name$"
    notification_hostdisplayname = "$host.display_name$"
    notification_servicename = "$service.name$"
    notification_serviceoutput = "$service.output$"
    notification_servicestate = "$service.state$"
    notification_useremail = "$user.email$"
    notification_servicedisplayname = "$service.display_name$"
    notification_icingaweb2url = "https://monitoring.fsfe.org"
  }
}


/**
 * The notification apply rules.
 *
 * Only applied if host/service objects have
 * the custom variable `notification` defined
 * and containing `mail` as key.
 *
 * Check `hosts.conf` for an example.
 */

apply Notification "mail-icingaadmin" to Host {
  import "mail-host-notification"
  user_groups = host.vars.notification.mail.groups
  users = host.vars.notification.mail.users

  interval = 24h

  vars.notification_logtosyslog = true
  vars.notification_from = "admin@fsfe.org"
  vars.notification_replyto = "system-hackers@lists.fsfe.org"

  assign where host.vars.notification.mail
}

apply Notification "mail-icingaadmin" to Service {
  import "mail-service-notification"
  user_groups = host.vars.notification.mail.groups
  users = host.vars.notification.mail.users

  // Longer interval for apt on physical hosts
  if (service.check_command == "check_apt" && "physical" in host.vars.groups) {
    interval = 168h
  } else {
    interval = 24h
  }

  vars.notification_logtosyslog = true
  vars.notification_from = "admin@fsfe.org"
  vars.notification_replyto = "system-hackers@lists.fsfe.org"

  assign where host.vars.notification.mail
}


/* Additional notifications specifically for problems connected to mail */
apply Notification "mailproblem-icingaadmin" to Host {
  import "mail-host-notification"
  user_groups = host.vars.notification.mailproblem.groups
  users = host.vars.notification.mailproblem.users

  interval = 24h

  vars.notification_logtosyslog = true
  vars.notification_from = "admin@fsfe.org"
  vars.notification_replyto = "system-hackers@lists.fsfe.org"

  assign where "mail" in host.vars.groups
}

apply Notification "mailproblem-icingaadmin" to Service {
  import "mail-service-notification"

  /* only send extra notifications to selected addresses if not apt problem */
  if (service.check_command == "check_apt") {
    user_groups = host.vars.notification.trashbin.groups
    users = host.vars.notification.trashbin.users
  } else {
    user_groups = host.vars.notification.mailproblem.groups
    users = host.vars.notification.mailproblem.users
  }

  interval = 24h

  vars.notification_logtosyslog = true
  vars.notification_from = "admin@fsfe.org"
  vars.notification_replyto = "system-hackers@lists.fsfe.org"

  assign where "mail" in host.vars.groups
}
