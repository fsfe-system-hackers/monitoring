#!/bin/bash
# SPDX-FileCopyrightText: Icinga 2 | (c) 2012 Icinga GmbH
# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
# SPDX-License-Identifier: GPL-2.0-or-later

PROG="`basename $0`"
ICINGA2HOST="`hostname`"
MAILBIN="msmtp -t -C /etc/msmtprc -a mailbox"

if [ -z "`which $MAILBIN`" ] ; then
  echo "$MAILBIN not found in \$PATH. Consider installing it."
  exit 1
fi

## Function helpers
Usage() {
cat << EOF

Required parameters:
  -d LONGDATETIME (\$icinga.long_date_time\$)
  -l HOSTNAME (\$host.name\$)
  -n HOSTDISPLAYNAME (\$host.display_name\$)
  -o HOSTOUTPUT (\$host.output\$)
  -r USEREMAIL (\$user.email\$)
  -s HOSTSTATE (\$host.state\$)
  -t NOTIFICATIONTYPE (\$notification.type\$)

Optional parameters:
  -4 HOSTADDRESS (\$address\$)
  -6 HOSTADDRESS6 (\$address6\$)
  -b NOTIFICATIONAUTHORNAME (\$notification.author\$)
  -c NOTIFICATIONCOMMENT (\$notification.comment\$)
  -i ICINGAWEB2URL (\$notification_icingaweb2url\$, Default: unset)
  -f MAILFROM (\$notification_mailfrom\$, requires GNU mailutils (Debian/Ubuntu) or mailx (RHEL/SUSE))
  -v (\$notification_sendtosyslog\$, Default: false)

EOF
}

Help() {
  Usage;
  exit 0;
}

Error() {
  if [ "$1" ]; then
    echo $1
  fi
  Usage;
  exit 1;
}

# Snippet Start
# SPDX-FileCopyrightText: Brian White (brian@aljex.com)
# SPDX-License-Identifier: MIT
urlencode() {
  local LANG=C i=0 c e s="$1"

  while [ $i -lt ${#1} ]; do
    [ "$i" -eq 0 ] || s="${s#?}"
    c=${s%"${s#?}"}
    [ -z "${c#[[:alnum:].~_-]}" ] || c=$(printf '%%%02X' "'$c")
    e="${e}${c}"
    i=$((i + 1))
  done
  echo "$e"
}
# Snippet End

## Main
while getopts 4:6::b:c:d:f:hi:l:n:o:r:R:s:t:v: opt
do
  case "$opt" in
    4) HOSTADDRESS=$OPTARG ;;
    6) HOSTADDRESS6=$OPTARG ;;
    b) NOTIFICATIONAUTHORNAME=$OPTARG ;;
    c) NOTIFICATIONCOMMENT=$OPTARG ;;
    d) LONGDATETIME=$OPTARG ;; # required
    f) MAILFROM=$OPTARG ;;
    h) Help ;;
    i) ICINGAWEB2URL=$OPTARG ;;
    l) HOSTNAME=$OPTARG ;; # required
    n) HOSTDISPLAYNAME=$OPTARG ;; # required
    o) HOSTOUTPUT=$OPTARG ;; # required
    r) USEREMAIL=$OPTARG ;; # required
    R) REPLYTO=$OPTARG ;;
    s) HOSTSTATE=$OPTARG ;; # required
    t) NOTIFICATIONTYPE=$OPTARG ;; # required
    v) VERBOSE=$OPTARG ;;
   \?) echo "ERROR: Invalid option -$OPTARG" >&2
       Error ;;
    :) echo "Missing option argument for -$OPTARG" >&2
       Error ;;
    *) echo "Unimplemented option: -$OPTARG" >&2
       Error ;;
  esac
done

shift $((OPTIND - 1))

## Keep formatting in sync with mail-service-notification.sh
for P in LONGDATETIME HOSTNAME HOSTDISPLAYNAME HOSTOUTPUT HOSTSTATE USEREMAIL NOTIFICATIONTYPE ; do
  eval "PAR=\$${P}"

  if [ ! "$PAR" ] ; then
    Error "Required parameter '$P' is missing."
  fi
done

## Build the message's subject
SUBJECT="[$HOSTDISPLAYNAME] $NOTIFICATIONTYPE: Host is $HOSTSTATE"

## Build the notification message
NOTIFICATION_MESSAGE=`cat << EOF
Message-ID: <icingamail-$(openssl rand -hex 12)@fsfe.org>
In-Reply-To: <icingamail-$HOSTDISPLAYNAME@fsfe.org>
Subject: $SUBJECT
From: $MAILFROM
To: $USEREMAIL
$(if [ -n "$REPLYTO" ]; then echo "Reply-To: $REPLYTO"; fi)

***** Host Monitoring on $ICINGA2HOST *****

$HOSTDISPLAYNAME is $HOSTSTATE

Info:    $HOSTOUTPUT

When:    $LONGDATETIME
Host:    $HOSTADDRESS
EOF
`

## Check whether author and comment was specified.
if [ -n "$NOTIFICATIONCOMMENT" ] ; then
  NOTIFICATION_MESSAGE="$NOTIFICATION_MESSAGE

Comment by $NOTIFICATIONAUTHORNAME:
  $NOTIFICATIONCOMMENT"
fi

## Check whether Icinga Web 2 URL was specified.
if [ -n "$ICINGAWEB2URL" ] ; then
  NOTIFICATION_MESSAGE="$NOTIFICATION_MESSAGE

View: $ICINGAWEB2URL/monitoring/host/show?host=$(urlencode "$HOSTNAME")"
fi

## Check whether verbose mode was enabled and log to syslog.
if [ "$VERBOSE" == true ] ; then
  logger "$PROG sends $SUBJECT => $USEREMAIL"
fi

## Send the mail using the $MAILBIN command.
if [ -n "$MAILFROM" ] ; then
  /usr/bin/printf "%b" "$NOTIFICATION_MESSAGE" | tr -d '\015' | $MAILBIN
else
  logger "MAILFROM (-f) not set!"
  exit 1
fi
